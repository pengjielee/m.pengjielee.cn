const webpack = require("webpack");

module.exports = {
	publicPath: 'dist',
  configureWebpack: {
    plugins: [
      new webpack.BannerPlugin({
        banner: () => {
          return "Last Build On " + new Date();
        }
      })
    ]
  }
};
